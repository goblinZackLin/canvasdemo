<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/grid',function(){
   return view('grid');
});

Route::get('/appCase',function(){
   return view('appCase');
});

Route::get('/blog',function(){
   return view('blog');
});
